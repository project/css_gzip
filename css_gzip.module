<?php

/**
 * @file
 * Gzips aggregated CSS files if CSS Optimization is turned on.
 */

define(CSS_GZIP_DIR, '/css/');

/**
 * Implementation of hook_help().
 *
 * @param $path
 *   A Drupal menu router path the help is being requested fo
 * @param $arg
 *   An array that corresponds to the return of the arg() function
 */
function css_gzip_help($path, $arg) {
  switch ($path) {
    case 'admin/help#css_gzip':
      $output = '<p>'. t('Option to gzip the aggregated CSS file when <em>CSS optimization</em> has been enabled in the <a href="@performance">Performance settings</a>. Requires .htaccess mod_rewrite (apache web server & clean urls).', array('@performance' => url('admin/settings/performance'))) .'</p>';
      return $output;
  }
}

/**
 * Implementation of hook_form_alter().
 *
 * @param &$form
 *   Nested array of form elements that comprise the form.
 * @param $form_state
 *   A keyed array containing the current state of the form.
 * @param $form_id
 *   String representing the name of the form itself. Typically this is the name of the function that generated the form.
 */
function css_gzip_form_alter(&$form, $form_state, $form_id) {
  if ($form_id == 'system_performance_settings') {
    $form['bandwidth_optimizations']['css_aggregator_gzip'] = array(
      '#type' => 'checkbox',
      '#title' => t('GZip CSS'),
      '#description' => t('Optionally <a href="@gzip">GZip</a> the aggregated CSS file to dramatically decrease its size.', array('@gzip' => 'http://en.wikipedia.org/wiki/Gzip')),
      '#default_value' => variable_get('css_aggregator_gzip', FALSE),
      '#weight' => 2,
    );
    $form['bandwidth_optimizations']['css_aggregator_gzip_no_htaccess'] = array(
      '#type' => 'checkbox',
      '#title' => t('GZip CSS: Do not generate .htaccess file'),
      '#description' => t('Sometimes your host does not like mutiple .htaccess files. Enable this to bypass htaccess file generation. Follow directions in the README.txt if this is effecting you.'),
      '#default_value' => variable_get('css_aggregator_gzip_no_htaccess', FALSE),
      '#weight' => 3,
    );
  }
}

/**
 * Implementation of hook_preprocess_page() or template_preprocess_page().
 *
 * Does not modify the pages contents, only gets info. Could probably use
 * drupal_get_css() and a different hook, if needed.
 *
 * @param &$variables
 *   Array containing various page elements.
 */
function css_gzip_preprocess_page(&$variables) {
  $htaccess = file_directory_path() . CSS_GZIP_DIR . '.htaccess';
  if (!variable_get('css_aggregator_gzip', FALSE) || variable_get('css_aggregator_gzip_no_htaccess', FALSE)) {
      css_gzip_remove_htaccess($htaccess);
    }
  if (!empty($variables['styles']) && variable_get('preprocess_css', FALSE) && variable_get('css_aggregator_gzip', FALSE)) {
    if (!variable_get('css_aggregator_gzip_no_htaccess', FALSE) && (file_exists($htaccess)==FALSE || variable_get('css_aggregator_gzip_htaccess_size', NULL)!=filesize($htaccess))) {
      css_gzip_create_htaccess($htaccess);
    }

    $css_files = css_gzip_file_list($variables);
    // Create the GZip file if it doesn't already exist.
    foreach ($css_files as $css_file) {
      if (file_exists(file_directory_path() . CSS_GZIP_DIR . $css_file) && !file_exists(file_directory_path() . CSS_GZIP_DIR . $css_file .'.gz')) {
        file_save_data(gzencode(file_get_contents(file_directory_path() . CSS_GZIP_DIR . $css_file), 9), file_directory_path() . CSS_GZIP_DIR . $css_file .'.gz', FILE_EXISTS_REPLACE);
      }
    }
  }
}

/**
 * Get list of css files for this page
 *
 * @param &$variables
 *   Array containing various page elements.
 */
function css_gzip_file_list($variables) {
  $path = base_path() . file_directory_path() . CSS_GZIP_DIR;
  $css_files=explode($path, $variables['styles']);
  array_shift($css_files);
  for ($i = 0; $i < count($css_files); $i++) {
    $css_files[$i]=array_shift(explode('.css', $css_files[$i])) . '.css';
  }
  return $css_files;
}

/**
 * Generate & write the .htaccess file inside the files/css dir.
 *
 * @param $htaccess
 *   Path and filename of the subdir .htaccess file.
 */
function css_gzip_create_htaccess($htaccess) {
  $htaccess_contents = <<<EOT
# Requires mod_mime to be enabled.
<IfModule mod_mime.c>
  # Send any files ending in .gz with x-gzip encoding in the header.
  AddEncoding x-gzip .gz
</IfModule>
# Gzip compressed css files are of the type 'text/css'.
<FilesMatch "\.css\.gz$">
  ForceType text/css
</FilesMatch>
<IfModule mod_rewrite.c>
  RewriteEngine on
  # Serve gzip compressed css files
  RewriteCond %{HTTP:Accept-encoding} gzip
  RewriteCond %{REQUEST_FILENAME}\.gz -s
  RewriteRule ^(.*)\.css $1\.css\.gz [L,QSA,T=text/css]
</IfModule>
EOT;
  file_save_data($htaccess_contents, $htaccess, FILE_EXISTS_REPLACE);
  variable_set('css_aggregator_gzip_htaccess_size', filesize($htaccess));
}

/**
 * Delete the .htaccess file inside the files/css dir.
 *
 * @param $htaccess
 *   Path and filename of the subdir .htaccess file.
 */
function css_gzip_remove_htaccess($htaccess) {
  if (file_exists($htaccess)) {
    file_delete($htaccess);
  }
}
